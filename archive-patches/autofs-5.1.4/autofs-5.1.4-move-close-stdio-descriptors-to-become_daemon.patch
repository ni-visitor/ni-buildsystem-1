autofs-5.1.4 - move close stdio descriptors to become_daemon()

From: Ian Kent <raven@themaw.net>

Move the stdio file descriptor close to the become_daemon() function
as closing these file descriptors, ie. detaching from ttys, is part
of the preperation for becoming a system daemon.

Signed-off-by: Ian Kent <raven@themaw.net>
---
 CHANGELOG          |    1 +
 daemon/automount.c |   27 ++++++++++++++++++++++++++-
 include/log.h      |    1 -
 lib/log.c          |   29 -----------------------------
 4 files changed, 27 insertions(+), 31 deletions(-)

diff --git a/CHANGELOG b/CHANGELOG
index 4c5f276e..4e1d9c98 100644
--- a/CHANGELOG
+++ b/CHANGELOG
@@ -46,6 +46,7 @@ xx/xx/2018 autofs-5.1.5
 - add NULL check in prepare_attempt_prefix().
 - update build info with systemd.
 - use flags for startup boolean options.
+- move close stdio descriptors to become_daemon().
 
 19/12/2017 autofs-5.1.4
 - fix spec file url.
diff --git a/daemon/automount.c b/daemon/automount.c
index d891562a..c1360ed6 100644
--- a/daemon/automount.c
+++ b/daemon/automount.c
@@ -1218,6 +1218,8 @@ static void become_daemon(unsigned int flags)
 		}
 		log_to_stderr();
 	} else {
+		int nullfd;
+
 		if (open_pipe(start_pipefd) < 0) {
 			fprintf(stderr, "%s: failed to create start_pipefd.\n",
 				program);
@@ -1261,7 +1263,30 @@ static void become_daemon(unsigned int flags)
 			close(start_pipefd[1]);
 			exit(*pst_stat);
 		}
-		log_to_syslog();
+
+		/* Redirect all our file descriptors to /dev/null */
+		nullfd = open("/dev/null", O_RDWR);
+		if (nullfd < 0) {
+			char *estr = strerror_r(errno, buf, MAX_ERR_BUF);
+			fprintf(stderr, "cannot open /dev/null: %s", estr);
+			res = write(start_pipefd[1], pst_stat, sizeof(*pst_stat));
+			close(start_pipefd[1]);
+			exit(*pst_stat);
+		}
+
+		if (dup2(nullfd, STDIN_FILENO) < 0 ||
+		    dup2(nullfd, STDOUT_FILENO) < 0 ||
+		    dup2(nullfd, STDERR_FILENO) < 0) {
+			char *estr = strerror_r(errno, buf, MAX_ERR_BUF);
+			fprintf(stderr,
+				"redirecting file descriptors failed: %s", estr);
+			res = write(start_pipefd[1], pst_stat, sizeof(*pst_stat));
+			close(start_pipefd[1]);
+			exit(*pst_stat);
+		}
+
+		open_log();
+		close(nullfd);
 	}
 
 	/* Write pid file if requested */
diff --git a/include/log.h b/include/log.h
index 7a394cbe..c9b17b3c 100644
--- a/include/log.h
+++ b/include/log.h
@@ -36,7 +36,6 @@ extern void set_log_debug_ap(struct autofs_point *ap);
 extern void set_mnt_logging(unsigned global_logopt);
 
 extern void open_log(void);
-extern void log_to_syslog(void);
 extern void log_to_stderr(void);
  
 extern void log_info(unsigned int, const char* msg, ...);
diff --git a/lib/log.c b/lib/log.c
index 41f95fe8..ca771d72 100644
--- a/lib/log.c
+++ b/lib/log.c
@@ -314,35 +314,6 @@ void open_log(void)
 	return;
 }
 
-void log_to_syslog(void)
-{
-	char buf[MAX_ERR_BUF];
-	int nullfd;
-
-	open_log();
-
-	/* Redirect all our file descriptors to /dev/null */
-	nullfd = open("/dev/null", O_RDWR);
-	if (nullfd < 0) {
-		char *estr = strerror_r(errno, buf, MAX_ERR_BUF);
-		fprintf(stderr, "cannot open /dev/null: %s", estr);
-		exit(1);
-	}
-
-	if (dup2(nullfd, STDIN_FILENO) < 0 ||
-	    dup2(nullfd, STDOUT_FILENO) < 0 ||
-	    dup2(nullfd, STDERR_FILENO) < 0) {
-		char *estr = strerror_r(errno, buf, MAX_ERR_BUF);
-		fprintf(stderr,
-			"redirecting file descriptors failed: %s", estr);
-		exit(1);
-	}
-
-	close(nullfd);
-
-	return;
-}
-
 void log_to_stderr(void)
 {
 	if (syslog_open) {
